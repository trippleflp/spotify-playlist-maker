/* eslint-disable camelcase */
/* eslint-disable func-names */


exports.handler = function (event, context, callback) {
  const my_client_id = '8bfa922d2e5a4ee0bb8186bdd1d8fc81';
  const redirect_uri = 'https://spohanna.netlify.com/.netlify/functions/spotify-redirect';
  const scopes = 'playlist-modify-public';

  const url = `\
https://accounts.spotify.com/authorize?response_type=code\
&client_id=${my_client_id}\
${(scopes ? `&scope=${encodeURIComponent(scopes)}` : '')}\
&redirect_uri=${encodeURIComponent(redirect_uri)}\
&show_dialog=true`;

  callback(null, {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      'Cache-Control': 'no-cache',
      'Content-Type': 'text/html',
    },
    body: JSON.stringify({ url }),
  });
};
