/* eslint-disable func-names */
/* eslint-disable import/prefer-default-export */
/* eslint-disable camelcase */
import axios from 'axios';

exports.handler = function (event, context, callback) {
  const client_secret = 'bb4a89a2c2a64fe6b88e9b8215cde18f';
  const client_id = '8bfa922d2e5a4ee0bb8186bdd1d8fc81';
  const redirect_uri = 'https://spohanna.netlify.com/.netlify/functions/spotify-redirect';

  const authenticateHeader = Buffer.from(`${client_id}:${client_secret}`, 'base64').toString('base64');

  const { referer } = event.headers;
  const params = event.queryStringParameters;
  const { code } = params;

  function getAccessToken() {
    const data = {
      grant_type: 'authorization_code',
      code,
      redirect_uri,
      // client_id,
      // client_secret,
    };

    return axios({
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: `Basic ${authenticateHeader}`,
      },
      url: 'https://accounts.spotify.com/api/token',
      data,
    }).then(res => res.data.access_token);
  }

  function sendToken(token) {
    callback(null, {
      statusCode: 302,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
        'Cache-Control': 'no-cache',
        Location: `${referer}?token=${token}`,
      },
      body: JSON.stringify({}),
    });
  }

  function sendError(error) {
    callback(null, {
      statusCode: 400,
      body: JSON.stringify({ error }),
    });
  }
  getAccessToken(code)
    // .then(console.log)
    .then(sendToken)
    .catch(sendError);
};
